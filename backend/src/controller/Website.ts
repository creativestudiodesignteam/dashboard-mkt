import { Request, Response } from 'express';
import PlanModel from '../models/Plan';
import Pterodactyl from '../services/Pterodactyl';
import dotEnv from 'dotenv';

class Website 
{
    public async minecraftServers(request: Request, response: Response) 
    {
        dotEnv.config();

        Pterodactyl.getAllServers((body) => {
            body = JSON.parse(body);

            let counter = 0;
            for (let i = 0; i < body.data.length; i++) {
                counter++;
            }

            response.status(200).json({
                success: true,
                servers: counter
            });
        });
    }

    public async getPlans(request: Request, response: Response) 
    {
        await PlanModel.sync({ force: false });

        response.status(200).json({
            success: true,
            plans: await PlanModel.findAll()
        });
    }
}

export default new Website();