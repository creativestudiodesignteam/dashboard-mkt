import { Request, Response } from 'express';
import dotEnv from 'dotenv';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';

import MasterModel from '../models/Master';

class Auth 
{
   public async loginUser(request: Request, response: Response) 
   {
      dotEnv.config();

      if (request.body.email === undefined || request.body.email === '' || request.body.password === undefined || request.body.password === '') {
         return response.status(200).json({
            "success": false,
            "message": "Complete todos os campos!"
         });
      }

      // await MasterModel.create({
      //    name: "Pedro Freitas",
      //    email: "devfreitas12@gmail.com",
      //    password: bcrypt.hashSync(request.body.password, 8),
      //    created_at: new Date(),
      //    updated_at: new Date()
      // });

      if (await MasterModel.count({ where: { email: request.body.email } }) === 0) {
         return response.status(200).json({
            "success": false,
            "message": "E-mail não cadastrado!"
         });
      }

      const Master: any = await MasterModel.findOne({where:{email: request.body.email}});

      if (!bcrypt.compareSync(request.body.password, Master.password)) {
         return response.status(200).json({
            "success": false,
            "message": "Senha incorreta!"
         });
      }
      jwt.sign({ id: Master.id }, process.env.SIG_TOKEN, (err, token) => {
         if (err) {
            return response.status(200).json({
               "success": false,
               "message": "Houve um erro interno."
            });
         }

         return response.status(200).json({
            "success": true,
            "message": "Logado com sucesso, redirecionando!",
            "master": token
         });
      }); 
   }
}

export default new Auth();