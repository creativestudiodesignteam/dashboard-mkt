import { Request, Response } from 'express';
import dotEnv from 'dotenv';

import User from '../models/Users';
import Sale from '../models/Sales';
import Customer from '../models/Customers';
import Provider from '../models/Providers';
import Products from '../models/Products';

class Data 
{
   public async users(request: Request, response: Response) 
   {
      dotEnv.config();

      return response.status(200).json({
         "success": true,
         "users": await User.count()
      });
   }

   public async sales(request: Request, response: Response) 
   {
      dotEnv.config();

      return response.status(200).json({
         "success": true,
         "sales": await Sale.count()
      });
   }

   public async salesPrice(request: Request, response: Response) 
   {
      dotEnv.config();

      const Sales: any = await Sale.findAll({where:{status: true}});
      let price = 0;
      Sales.forEach(item => {
         price = price+item.price;
      });

      return response.status(200).json({
         "success": true,
         "price": price
      });
   }

   public async usersCustomers(request: Request, response: Response) 
   {
      dotEnv.config();

      return response.status(200).json({
         "success": true,
         "customers": await Customer.count()
      });
   }

   public async usersProviders(request: Request, response: Response) 
   {
      dotEnv.config();

      return response.status(200).json({
         "success": true,
         "providers": await Provider.count()
      });
   }

   public async products(request: Request, response: Response) 
   {
      dotEnv.config();

      return response.status(200).json({
         "success": true,
         "products": await Products.count()
      });
   }
}

export default new Data();