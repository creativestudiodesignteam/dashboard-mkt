import Sequelize1, { Sequelize as SequelizeS, ConnectionError } from 'sequelize';
import dotEnv from 'dotenv';

class Sequelize 
{
    private connection: SequelizeS;

    async createConnection() 
    {
        dotEnv.config();
        this.connection = new SequelizeS(process.env.DB_DATABASE, process.env.DB_USER, process.env.DB_PASSWORD, {
            host: process.env.DB_HOST,
            dialect: 'postgres',
            logging: false
        });

        try {
            await this.connection.authenticate();
        } catch (error) {
            console.log("Error to connect to database!");
            console.log(error);
        }
    }

    public getConnection() : SequelizeS 
    {
        this.createConnection();
        return this.connection;
    }
}

export default new Sequelize();