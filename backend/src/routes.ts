import { Router } from "express";
import Auth from './controller/Auth';
import Data from './controller/Data';

const routes = Router();

/*
*  Routes auth
* */
routes.post('/auth/login', Auth.loginUser);

/*
*  Routes data
* */
routes.get('/data/users', Data.users);
routes.get('/data/users/customers', Data.usersCustomers);
routes.get('/data/users/providers', Data.usersProviders);
routes.get('/data/sales', Data.sales);
routes.get('/data/sales/prices', Data.salesPrice);
routes.get('/data/products', Data.products);

export default routes;