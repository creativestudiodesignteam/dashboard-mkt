import express from 'express';
import dotEnv from 'dotenv';
import Routes from './routes';
import jwt from 'jsonwebtoken';
import bodyParser from 'body-parser';
import Sequelize from './services/Sequelize';

const server = express();
dotEnv.config();

server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json());
server.use(function (request, response, next) {
    response.header("Access-Control-Allow-Origin", "*");
    response.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
    response.header('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');
    response.header('Access-Control-Allow-Credentials', 'true');
    next();
});

server.use(async function (request, response, next) {
    if (request.headers.authorization) {
        if (request.headers.authorization.split(' ')[1] === process.env.TOKEN) {
            next();
        }else {
            try {
                jwt.verify(request.headers.authorization.split(' ')[1], process.env.SIG_TOKEN);
                next();
            }catch (e) {
                response.status(200).json({
                    "error": "Invalid authorization token"
                });
            }
        }
    }else {
        response.status(200).json({
            "error": "Invalid authorization token"
        });
    }
});

server.use(Routes);

server.listen(8082);