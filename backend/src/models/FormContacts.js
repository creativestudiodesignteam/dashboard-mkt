import Sequelize, { Model } from 'sequelize';

class FormContacts extends Model {
    static init(sequelize) {
        super.init({
            name: Sequelize.STRING,
            tel: Sequelize.STRING,
            email: Sequelize.STRING,
            message: Sequelize.STRING,
            status: Sequelize.BOOLEAN,
        }, {
            sequelize,
        });
        return this;
    }
}

export default FormContacts;