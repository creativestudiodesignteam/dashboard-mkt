import Sequelize, { Model } from 'sequelize';

class Prodsubcats extends Model {
  static init(sequelize) {
    super.init(
      {
        status: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.Subcategories, {
      foreignKey: 'fk_subcategories',
      as: 'subcategories',
    });
    this.belongsTo(models.Products, {
      foreignKey: 'fk_products',
      as: 'products',
    });
  }
}


export default Prodsubcats;
