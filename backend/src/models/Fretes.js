import Sequelize, { Model } from 'sequelize';

class Fretes extends Model {
  static init(sequelize) {
    super.init(
      {
        /* length height width weight_unit weight situation */
        valor: Sequelize.FLOAT,
        tipo: Sequelize.STRING,
        cod_servico: Sequelize.STRING,
        prazo: Sequelize.STRING,
        status: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.User, {
      foreignKey: 'fk_users',
      as: 'user',
    });
  }
}

export default Fretes;
