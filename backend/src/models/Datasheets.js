import Sequelize, { Model } from 'sequelize';

class Datasheets extends Model {
  static init(sequelize) {
    super.init(
      {
        /* cpf telephone gender birthday */
        name: Sequelize.STRING,
        description: Sequelize.STRING,
        status: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.Products, {
      foreignKey: 'fk_products',
      as: 'products',
    });
  }
}

export default Datasheets;
