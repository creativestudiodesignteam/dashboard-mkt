import Sequelize, { Model, DataTypes } from 'sequelize';

import SequelizeCon from '../services/Sequelize';

const sequelizeCon = SequelizeCon.getConnection();


class Sale extends Model {}

Sale.init({
	hash: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    amount: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    quota: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    price: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    quota: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    payment_type: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    status: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
    }
}, {
    sequelize: sequelizeCon,
    timestamps: false,
    modelName: 'sales'
});

// class Sales extends Model {
//   static init(sequelize) {

//     super.init(
//       {
//         hash: Sequelize.STRING,
//         amount: Sequelize.INTEGER,
//         quota: Sequelize.INTEGER,
//         price: Sequelize.INTEGER,
//         payment_type: Sequelize.INTEGER,
//         status: Sequelize.BOOLEAN,
//         created_at: Sequelize.DATE,
//         canceled_at: Sequelize.DATE,
//       },
//       {
//         sequelize,
//       }
//     );
//     return this;
//   }
//   static associate(models) {
//     this.belongsTo(models.Grids, {
//       foreignKey: 'fk_grids',
//       as: 'grids',
//     });

//     this.belongsTo(models.User, {
//       foreignKey: 'fk_users',
//       as: 'users',
//     });
//     this.belongsTo(models.Providers, {
//       foreignKey: 'fk_provider',
//       as: 'providers',
//     });
//     this.belongsTo(models.Payments, {
//       foreignKey: 'fk_payments',
//       as: 'payments',
//     });
//     this.belongsTo(models.Fretes, {
//       foreignKey: 'fk_fretes',
//       as: 'fretes',
//     });
//   }

// }

export default Sale;
