import Sequelize, { Model } from 'sequelize';

class Subcategories extends Model {
  static init(sequelize) {
    super.init(
      {
        name: Sequelize.STRING,
        status: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.Categories, {
      foreignKey: 'fk_categories',
      as: 'categories',
    });
  }
}

export default Subcategories;
