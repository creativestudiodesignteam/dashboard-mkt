import Sequelize, { Model, DataTypes } from 'sequelize';

import SequelizeCon from '../services/Sequelize';

const sequelizeCon = SequelizeCon.getConnection();


class Customer extends Model {}

Customer.init({
	name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  cpf: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  rg: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  telephone: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  birthday: {
    type: DataTypes.STRING,
    allowNull: false,
  },
    status: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
    }
}, {
    sequelize: sequelizeCon,
    timestamps: false,
    modelName: 'customers'
});

// class Customers extends Model {
//   static init(sequelize) {
//     super.init(
//       {
//         name: Sequelize.STRING,
//         cpf: Sequelize.STRING,
//         rg: Sequelize.STRING,
//         telephone: Sequelize.STRING,
//         birthday: Sequelize.DATE,
//         status: Sequelize.BOOLEAN,
//       },
//       {
//         sequelize,
//       }
//     );
//     return this;
//   }
//   static associate(models) {
//     this.belongsTo(models.User, {
//       foreignKey: 'fk_users',
//       as: 'users',
//     });
//   }
// }

export default Customer;
