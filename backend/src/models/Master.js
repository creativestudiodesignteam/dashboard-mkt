import Sequelize, { Model, DataTypes } from 'sequelize';
import bcrypt from 'bcryptjs';
import SequelizeCon from '../services/Sequelize';

const sequelizeCon = SequelizeCon.getConnection();

class Master extends Model {}

Master.init({
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false,
	 },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
    }
}, {
    sequelize: sequelizeCon,
    timestamps: false,
    modelName: 'masters'
});

Master.checkPassword = function(password) {
    return bcrypt.compare(password, this.password);
}

// class Master extends Model {
// 	static init(sequelize) {
// 		super.init(
// 			{
// 				name: DataTypes.STRING,
// 				email: DataTypes.STRING,
// 				password: DataTypes.STRING
// 			},
// 			{
// 				sequelize,
// 				tableName: 'masters'
// 			}
// 		);
		
// 		return this;
// 	}

// 	checkPassword(password) {
// 		return bcrypt.compare(password, this.password_hash);
// 	}
	
// }

export default Master;
