import Sequelize, { Model } from 'sequelize';

class Taxes extends Model {
  static init(sequelize) {

    super.init(
      {
        tax_type_name: Sequelize.STRING,
        tax: Sequelize.FLOAT,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.MerchantSplit, {
      foreignKey: 'fk_merchant_split',
      as: 'merchant_split',
    });
  }

}

export default Taxes;
