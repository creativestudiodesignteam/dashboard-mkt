import Sequelize, { Model } from 'sequelize';

class Payments extends Model {
  static init(sequelize) {

    super.init(
      {
        code: Sequelize.STRING,
        method: Sequelize.STRING,
        url: Sequelize.STRING,
        references: Sequelize.STRING,
        situations: Sequelize.STRING,
        status: Sequelize.BOOLEAN,
        created_at: Sequelize.DATE,
      },
      {
        sequelize,
      }
    );
    return this;
  }
}

export default Payments;
