import Sequelize, { Model } from 'sequelize';

class Options extends Model {
  static init(sequelize) {
    super.init(
      {
        /* length height width weight_unit weight situation */
        name: Sequelize.STRING,
        type: Sequelize.STRING,
        status: Sequelize.BOOLEAN,
        default: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.User, {
      foreignKey: 'fk_users',
      as: 'user',
    });
  }
}

export default Options;
