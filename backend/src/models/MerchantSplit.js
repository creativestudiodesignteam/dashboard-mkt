import Sequelize, { Model } from 'sequelize';

class MerchantSplit extends Model {
  static init(sequelize) {

    super.init(
      {
        payment_method_code: Sequelize.INTEGER,
        is_sub_account_tax_payer: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.Providers, {
      foreignKey: 'fk_providers',
      as: 'providers',
    });
  }

}

export default MerchantSplit;
