import Sequelize, { Model } from 'sequelize';

class Grids extends Model {
  static init(sequelize) {
    super.init(
      {
        /* length height width weight_unit weight situation */
        amount: Sequelize.STRING,
        price: Sequelize.STRING,
        status: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.User, {
      foreignKey: 'fk_users',
      as: 'user',
    });
    this.belongsTo(models.Products, {
      foreignKey: 'fk_products',
      as: 'products',
    });
  }
}

export default Grids;
