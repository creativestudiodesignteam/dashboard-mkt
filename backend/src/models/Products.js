import Sequelize, { Model, DataTypes } from 'sequelize';

import SequelizeCon from '../services/Sequelize';

const sequelizeCon = SequelizeCon.getConnection();

class Product extends Model {}

Product.init({
	name: {
    type: DataTypes.STRING,
    allowNull: false,
  },

  description: {
    type: DataTypes.STRING,
    allowNull: false,
  },
    status: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
    }
}, {
    sequelize: sequelizeCon,
    timestamps: false,
    modelName: 'products'
});

// class Products extends Model {
//   static init(sequelize) {
//     super.init(
//       {
//         /* name description */
//         name: Sequelize.STRING,
//         description: Sequelize.STRING,
//         brand: Sequelize.STRING,
//         model: Sequelize.STRING,
//         sku: Sequelize.STRING,
//         condition: Sequelize.STRING,
//         status: Sequelize.BOOLEAN,
//       },
//       {
//         sequelize,
//       }
//     );
//     return this;
//   }
//   static associate(models) {
//     this.belongsTo(models.File, {
//       foreignKey: 'fk_thumb',
//       as: 'file',
//     });
//     this.belongsTo(models.Details, {
//       foreignKey: 'fk_details',
//       as: 'details',
//     });
//     this.belongsTo(models.Categories, {
//       foreignKey: 'fk_categories',
//       as: 'categories',
//     });
//     /*     this.belongsTo(models.Subcategories, {
//           foreignKey: 'fk_subcategories',
//           as: 'subcategories',
//         }); */
//     this.belongsTo(models.User, {
//       foreignKey: 'fk_users',
//       as: 'users',
//     });
//   }
// }

export default Product;
