import React from "react"
import axios from "axios"
import { Row, Col } from "reactstrap"
import SubscribersGained from "../../ui-elements/cards/statistics/SubscriberGained"
import RevenueGenerated from "../../ui-elements/cards/statistics/RevenueGenerated"
import QuaterlySales from "../../ui-elements/cards/statistics/QuaterlySales"
import OrdersReceived from "../../ui-elements/cards/statistics/OrdersReceived"
import RevenueChart from "../../ui-elements/cards/analytics/Revenue"
//import GoalOverview from "../../ui-elements/cards/analytics/GoalOverview"
//import BrowserStats from "../../ui-elements/cards/analytics/BrowserStatistics"
//import ClientRetention from "../../ui-elements/cards/analytics/ClientRetention"
// import SessionByDevice from "../../ui-elements/cards/analytics/SessionByDevice"
import CustomersChart from "../../ui-elements/cards/analytics/Customers"
// import ChatWidget from "../../../components/@vuexy/chatWidget/ChatWidget"

import "../../../assets/scss/plugins/charts/apex-charts.scss"

let $primary = "#7367F0",
  //$success = "#28C76F",
  $danger = "#EA5455",
  $warning = "#FF9F43",
  $primary_light = "#9c8cfc",
  $warning_light = "#FFC085",
  $danger_light = "#f29292",
  $stroke_color = "#b9c3cd",
  $label_color = "#e7eef7"

class EcommerceDashboard extends React.Component {

  state = {
    users: 0,
    sales: 0,
    salesPrices: 0,
    customers: 0,
    providers: 0,
    products: 0,
  };

  componentWillMount() {
    const headers = {
      "Content-Type": "application/json",
      "Authorization": "Bearer "+JSON.parse(window.localStorage.getItem('pm_log_master'))
    }

    axios.get("http://localhost:8082/data/users", {headers})
    .then((data) => {
      this.setState({ users: data.data.users });
    });
    axios.get("http://localhost:8082/data/sales", {headers})
    .then((data) => {
      this.setState({ sales: data.data.sales });
    });
    axios.get("http://localhost:8082/data/sales/prices", {headers})
    .then((data) => {
      this.setState({ salesPrices: data.data.price });
    });
    axios.get("http://localhost:8082/data/products", {headers})
    .then((data) => {
      this.setState({ products: data.data.products });
    });
    axios.get("http://localhost:8082/data/users/customers", {headers})
    .then((data) => {
      this.setState({ customers: data.data.customers });
    });
    axios.get("http://localhost:8082/data/users/providers", {headers})
    .then((data) => {
      this.setState({ providers: data.data.providers });
    });
  }

  render() {
    return (
      <React.Fragment>
        <Row className="match-height">
          <Col lg="3" md="6" sm="6">
            <SubscribersGained users={this.state.users} />
          </Col>
          <Col lg="3" md="6" sm="6">
            <QuaterlySales sales={this.state.sales} />
          </Col>
          <Col lg="3" md="6" sm="6">
            <RevenueGenerated price={this.state.salesPrices} />
          </Col>
          <Col lg="3" md="6" sm="6">
            <OrdersReceived products={this.state.products} />
          </Col>
        </Row>
        <Row className="match-height">
          <Col lg="8" md="6" sm="12">
            <RevenueChart
              primary={$primary}
              dangerLight={$danger_light}
              strokeColor={$stroke_color}
              labelColor={$label_color}
              price={this.state.salesPrices}
            />
          </Col>
          <Col lg="4" md="6" sm="12">
            <CustomersChart
              primary={$primary}
              warning={$warning}
              danger={$danger}
              primaryLight={$primary_light}
              warningLight={$warning_light}
              dangerLight={$danger_light}
              customers={this.state.customers}
              providers={this.state.providers}
            />
          </Col>
        </Row>
        {/* <Row className="match-height">
          <Col lg="4" md="6" sm="12">
            <BrowserStats />
          </Col>
          <Col lg="8" md="6" sm="12">
            <ClientRetention
              strokeColor={$stroke_color}
              primary={$primary}
              danger={$danger}
              labelColor={$label_color}
            />
          </Col>
        </Row> */}
      </React.Fragment>
    )
  }
}

export default EcommerceDashboard
