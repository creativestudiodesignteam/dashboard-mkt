import React from "react"
// import { Link } from "react-router-dom"
import { CardBody, FormGroup, Form, Input, Button, Label } from "reactstrap"
import Checkbox from "../../../../components/@vuexy/checkbox/CheckboxesVuexy"
import { Mail, Lock, Check } from "react-feather"
import { loginWithJWT } from "../../../../redux/actions/auth/loginActions"
import { connect } from "react-redux"
import axios from "axios";
// import { history } from "../../../../history"

class LoginJWT extends React.Component {
  state = {
    email: "",
    password: "",
    remember: false
  }

  handleLogin = e => {
    e.preventDefault()
    const headers = {
      "Content-Type": "application/json",
      "Authorization": "Bearer "+process.env.REACT_APP_TOKEN
    }

    axios.post("http://localhost:8082/auth/login", {
      email: this.state.email,
      password: this.state.password
    }, {headers})
    .then((data) => {
      if (!data.data.success) {
        alert(data.data.message);
      }else {
      
        window.localStorage.setItem('pm_log_master', JSON.stringify(data.data.master))
        window.location.href = '/#';
      }
    });

  }
  render() {
    return (
      <React.Fragment>
        <CardBody style={{marginTop: 70}} className="pt-1">
          <Form action="/" onSubmit={this.handleLogin}>
            <FormGroup className="form-label-group position-relative has-icon-left">
              <Input
                type="email"
                placeholder="E-mail"
                onChange={e => this.setState({ email: e.target.value })}
                required
              />
              <div className="form-control-position">
                <Mail size={15} />
              </div>
              <Label>E-mail</Label>
            </FormGroup>
            <FormGroup className="form-label-group position-relative has-icon-left">
              <Input
                type="password"
                placeholder="Senha"
                onChange={e => this.setState({ password: e.target.value })}
                required
              />
              <div className="form-control-position">
                <Lock size={15} />
              </div>
              <Label>Senha</Label>
            </FormGroup>
            <FormGroup className="d-flex justify-content-between align-items-center">
              <Checkbox
                color="primary"
                icon={<Check className="vx-icon" size={16} />}
                label="Remember me"
                defaultChecked={false}
                onChange={this.handleRemember}
              />
              <div className="float-right">
                {/* <Link to="/auth/forgot-password">Esqueceu a senha?</Link> */}
              </div>
            </FormGroup>
            <div className="d-flex justify-content-between">
            
              <Button.Ripple color="primary" type="submit">
                Acessar 
              </Button.Ripple>
            </div>
          </Form>
        </CardBody>
      </React.Fragment>
    )
  }
}
const mapStateToProps = state => {
  return {
    values: state.auth.login
  }
}
export default connect(mapStateToProps, { loginWithJWT })(LoginJWT)
